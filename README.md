# Matter Buildpack

This buildpack:

* fetches a artifact specified in `matter-buildpack.json`
* stores it in a local cache
* unpacks it into the build directory

We use this buildpack in conjunction with our slug-style artifacts.

A slug artifact is a tarball containing all of an application's dependencies.